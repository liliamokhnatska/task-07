package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;

	private Connection connection;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			Properties prop = new Properties();
			try {
				prop.load(new FileInputStream("app.properties"));
				DBManager manager = new DBManager();
				manager.setConnection(DriverManager.getConnection(prop.getProperty("connection.url")));
				instance = manager;
			} catch (IOException | SQLException e) {
				throw new IllegalArgumentException("something wrong with db");
			}
		}
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try (Statement stmt = connection.createStatement()) {
			ResultSet resultSet = stmt.executeQuery("SELECT * FROM users");
			while (resultSet.next()) {
			users.add(new User(resultSet.getString("login")));
			}
		} catch (SQLException throwable) {
			throwable.printStackTrace();
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		String query = "INSERT INTO users VALUES (DEFAULT, '" + user.getLogin() + "')";
		try (Statement stmt = connection.createStatement()) {
			stmt.executeUpdate(query);
		} catch (SQLException  throwable) {
			throwable.printStackTrace();
		}
		return true;
	}
	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try (Statement stmt = connection.createStatement()) {
			ResultSet resultSet = stmt.executeQuery("SELECT * FROM teams");
			while (resultSet.next()) {
				teams.add(new Team(resultSet.getString("name")));
			}
		} catch (SQLException throwable) {
			throwable.printStackTrace();
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		String query = "INSERT INTO teams VALUES (DEFAULT, '" + team.getName() + "')";
		try (Statement stmt = connection.createStatement()) {
			stmt.executeUpdate(query);
		} catch (SQLException throwable) {
			throwable.printStackTrace();
		}
		return true;
	}
	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		User userInDb = getUser(user.getLogin());
		Map<Team, User> userTeamMap = new HashMap<>();
		for (Team team: teams){
			Team teamInDb = getTeam(team.getName());
			userTeamMap.put(teamInDb, userInDb);
		}

		Map<Team, User> userTeamMapSavedInDB = new HashMap<>();
		boolean isFailed = false;
		for(Map.Entry<Team, User> entry : userTeamMap.entrySet()) {
			String query = "INSERT INTO users_teams VALUES (" + entry.getValue().getId() + ", " + entry.getKey().getId() + ")";
			try (Statement stmt = connection.createStatement()) {
				stmt.executeUpdate(query);
			} catch (SQLException throwable) {
				isFailed = true;
				break;
			}
			userTeamMapSavedInDB.put(entry.getKey(), entry.getValue());
		}

		if (isFailed) {
			for(Map.Entry<Team, User> entry : userTeamMapSavedInDB.entrySet()) {
				try {
					deleteUserTeam(entry.getValue(), entry.getKey());

				} catch (Exception throwable){

				}
			}
			throw new DBException("Can not delete", new SQLException());
		}

		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		User userInDb = getUser(user.getLogin());
		List<Team> teams = new ArrayList<>();
		try (Statement stmt = connection.createStatement()) {
			ResultSet resultSet = stmt.executeQuery("SELECT * FROM users_teams WHERE user_id = " + userInDb.getId());
			List<Integer> teamsIds = new ArrayList<>();
			while (resultSet.next()) {
				teamsIds.add(resultSet.getInt("team_id"));
			}
			for (Integer teamId : teamsIds) {
				teams.add(getTeamById(teamId));
			}
		} catch (SQLException throwable) {
			throwable.printStackTrace();
		}
		return teams;
	}



	public boolean deleteUsers(User... users) throws DBException {
		for (User user : users) {
			deleteUser(user);
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		try (Statement stmt = connection.createStatement()) {
			ResultSet resultSet = stmt.executeQuery("SELECT * FROM users WHERE login = '" + login + "'");
			if (resultSet.next()) {
				return new User(resultSet.getInt("id"), resultSet.getString("login"));
			}
		} catch (SQLException throwable) {
			throwable.printStackTrace();
		}
		return null;
	}

	public Team getTeam(String name) throws DBException {
		try (Statement stmt = connection.createStatement()) {
			ResultSet resultSet = stmt.executeQuery("SELECT * FROM teams WHERE name = '" + name + "'");
			if (resultSet.next()) {
				return new Team(resultSet.getInt("id"), resultSet.getString("name"));
			}
		} catch (SQLException throwable) {
			throwable.printStackTrace();
		}
		return null;
	}

	private Team getTeamById(Integer id) {
		try (Statement stmt = connection.createStatement()) {
			ResultSet resultSet = stmt.executeQuery("SELECT * FROM teams WHERE id = " + id);
			if (resultSet.next()) {
				return new Team(resultSet.getInt("id"), resultSet.getString("name"));
			}
		} catch (SQLException throwable) {
			throwable.printStackTrace();
		}
		return null;
	}








	public boolean deleteTeam(Team team) throws DBException {
		String query = "DELETE FROM teams WHERE name = '" + team.getName() + "'";
		try (Statement stmt = connection.createStatement()) {
			stmt.executeUpdate(query);
		} catch (SQLException throwables) {
			return false;
		}
		return true;
	}

	public boolean deleteUser(User user) throws DBException {
		String query = "DELETE FROM users WHERE login = '" + user.getLogin() + "'";
		try (Statement stmt = connection.createStatement()) {
			stmt.executeUpdate(query);
		} catch (SQLException throwables) {
			throw new DBException("Can not delete", throwables);
		}
		return true;
	}

	public boolean deleteUserTeam(User user, Team team) throws DBException {
		String query = "DELETE FROM users_teams WHERE user_id = " + user.getId() + " AND team_id = " + team.getId() ;
		try (Statement stmt = connection.createStatement()) {
			stmt.executeUpdate(query);
		} catch (SQLException throwable) {
			throw new DBException("Can not delete", throwable);
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		String query = "UPDATE teams SET name = '" + team.getName() + "' WHERE id =" + team.getId();
		try (Statement stmt = connection.createStatement()) {
			stmt.executeUpdate(query);
		} catch (SQLException throwables) {
			throw new DBException("Can not update", throwables);
		}
		return true;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}



	private User getUserById(Integer id) {
		try (Statement stmt = connection.createStatement()) {
			ResultSet resultSet = stmt.executeQuery("SELECT * FROM users WHERE id = " + id);
			if (resultSet.next()) {
				return new User(resultSet.getInt("id"), resultSet.getString("login"));
			}
		} catch (SQLException throwables) {
			return null;
		}

		return null;
	}
}
